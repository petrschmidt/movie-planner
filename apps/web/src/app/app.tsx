import styled from 'styled-components';

import { GlobalStyle, SearchBar } from '@movie-planner/ui';

const StyledApp = styled.div`
  width: 100%;
  height: 100vh;
  overflow-y: hidden;
  background: var(--bg-gradient);
`;

const StyledAppWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 64px;
`;

export function App() {
  return (
    <StyledApp>
      <GlobalStyle />
      <StyledAppWrapper>
        <SearchBar placeholder={'Type to search...'} />
      </StyledAppWrapper>
    </StyledApp>
  );
}

export default App;
