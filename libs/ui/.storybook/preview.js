import React from 'react';
import { GlobalStyle } from '@movie-planner/ui';

export const decorators = [
  (Story) => (
    <>
      <GlobalStyle />
      <Story />
    </>
  )
];
