import styled from 'styled-components';
import { GlobalStyle } from '../index';

/* eslint-disable-next-line */
export interface UiProps {}

const StyledUi = styled.div`
  color: pink;
`;

export function Ui(props: UiProps) {
  return (
    <StyledUi>
      <GlobalStyle />
      <h1>Welcome to Ui!</h1>
    </StyledUi>
  );
}

export default Ui;
