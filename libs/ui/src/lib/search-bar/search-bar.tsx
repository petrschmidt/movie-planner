import styled from 'styled-components';

/* eslint-disable-next-line */
export interface SearchBarProps {
  placeholder?: string;
}

const StyledSearchBar = styled.input`
  padding: 20px 16px;
  border-radius: var(--border-radius-large);
  background: var(--color-white-light);
`;

export function SearchBar({ placeholder }: SearchBarProps) {
  return (
    <StyledSearchBar placeholder={placeholder} />
  );
}

export default SearchBar;
