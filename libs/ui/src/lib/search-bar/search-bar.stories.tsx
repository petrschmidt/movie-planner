import { Story, Meta } from '@storybook/react';
import { SearchBar, SearchBarProps } from './search-bar';

export default {
  component: SearchBar,
  title: 'SearchBar'
} as Meta;

const Template: Story<SearchBarProps> = (args) => (
  <div style={{ background: 'var(--color-blue)', padding: "40px" }}>
    <SearchBar placeholder={'Type to search...'} {...args} />
  </div>
);

export const Primary = Template.bind({});
Primary.args = {};
