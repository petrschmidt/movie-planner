import { css } from 'styled-components';

const montserrat = css`
  @font-face {
    font-family: 'Montserrat';
    src: local('Montserrat'),
      url('./montserrat/montserrat-regular-webfont.woff2') format('woff2'),
      url('./montserrat/montserrat-regular-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'Montserrat';
    src: local('Montserrat'),
      url('./montserrat/montserrat-light-webfont.woff2') format('woff2'),
      url('./montserrat/montserrat-light-webfont.woff') format('woff');
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: 'Montserrat';
    src: local('Montserrat'),
      url('./montserrat/montserrat-semibold-webfont.woff2') format('woff2'),
      url('./montserrat/montserrat-semibold-webfont.woff') format('woff');
    font-weight: 600;
    font-style: normal;
  }

  @font-face {
    font-family: 'Montserrat';
    src: local('Montserrat'),
      url('./montserrat/montserrat-bold-webfont.woff2') format('woff2'),
      url('./montserrat/montserrat-bold-webfont.woff') format('woff');
    font-weight: bold;
    font-style: normal;
  }
`;

export const fonts = {
  montserrat
};
