import { createGlobalStyle } from 'styled-components';
import { normalize } from "styled-normalize";
import { fonts } from './assets/fonts';

export * from './lib/search-bar/search-bar';
export * from './lib/ui';

export const GlobalStyle = createGlobalStyle`
  ${normalize}
  ${fonts.montserrat}

  :root {
    font-size: 16px;

    --color-black: #000814;
    --color-accent: #FFC300;
    --color-blue: #001D3D;
    --color-white: #FFFFFF;
    --color-white-light: rgba(255, 255, 255, 0.05);

    --border-radius-normal: 10px;
    --border-radius-large: 18px;

    --bg-gradient: linear-gradient(60deg, var(--color-black), var(--color-blue));
  }

  body {
    font-family: Montserrat, sans-serif;
    margin: 0;

    * {
      box-sizing: border-box;
    }
  }

  input, button, textarea {
    outline: none;
    border: none;
  }

  ::selection {
    background: var(--color-accent);
    color: var(--color-black);
  }
`;
